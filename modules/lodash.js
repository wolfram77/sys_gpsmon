/* -------------
 * Lodash Mixins
 * -------------
 * 
 * Provides lodash with its mixins.
 * 
 * License:
 * Gps Mon, Copyright (c) 2010-2014, Subhajit Sahu, All Rights Reserved.
 * see: /LICENSE.txt for details.
 */



// dependencies
var _ = require('lodash');



// module loading mixin [_.load(module, scope)]
_.mixin({'load': function(mod, scope) {
    
    // load module
    var dep = [];
    var md = require(mod);
    
    // if it is a root library
    if(mod.slice(0,2) !== './') return scope[mod] = md;
    
    // get dependencies
    for(var i=md.length-2; i>=0; i--) {
        dep[i] = scope[md[i]];
    }
    
    // call the fn after loading modules
    return scope[mod.slice(2)] = md[md.length-1].apply(scope, dep);
}});



// assign only mixin [_.assignOnly(dest, src, [properties])]
_.mixin({'assignOnly': function(dest, src, props) {
    for(var i=props.length-1; i>=0; i--) {
        dest[props[i]] = src[props[i]];
    }
    return dest;
}});



module.exports = function() {
    return _;
};
