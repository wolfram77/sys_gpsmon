/* --------------
 * Tracker Module
 * --------------
 * 
 * This is the module for vehicle tracking.
 * 
 * License:
 * Gps Mon, Copyright (c) 2010-2014, Subhajit Sahu, All Rights Reserved.
 * see: /LICENSE.txt for details.
 */



module.exports = ['_', 'fs', 'log', 'config', 'moment', function(_, fs, log, config, moment) {
	// initialize
	var o = {};



	// status (local & global), history
	o.status = {};
	o.history = {};
	o.gstatus = {
		'count': 0
	};
    


	// save info to file
	o.save = function() {
		var data = _.assignOnly({}, o, ['status', 'history', 'gstatus']);
		fs.writeFile(config.track.file, JSON.stringify(data), function(err) {
			if(err) log.write('Failed to save Track file');
			else log.write('Track file saved');
		});
		return 'ok';
	};



	// load info from file
	o.load = function() {
		try {
			_.assign(o, JSON.parse(fs.readFileSync(config.track.file, 'utf8')));
			log.write('Track file loaded');
		}
		catch(e) {
			log.write('Failed to load track file');
		}
		return 'ok';
	};



	// calculate distance between two points
	o.calcDist = function(p1, p2) {
		var cy1 = Math.cos(p1.lat);
		var cy2 = Math.cos(p2.lat);
		var sdy = Math.sin((p2.lat-p1.lat)*0.5);
		var sdx = Math.sin((p2.lon-p1.lon)*0.5);
		var a = sdy*sdy + cy1*cy2*sdx*sdx;
		var c = 2*Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		return c*6378.1*1000; // radius of earth
	};



	// update driver photo
	o.updatePhoto = function(id, photo) {

		// remove if desired
		if(!photo) {
			fs.unlink('assets/data/tracker/'+id+'/driver.'+o.status[id].driver.photo);
			log.write('Vehicle ['+id+'] driver photo deleted');
		}

		// photo to be updated
		else {

			// check if data is valid
			if(!photo.data || typeof photo.type !== 'string') return 'invalid photo';
			if(photo.data.length > config.photo.maxSize) return 'too big photo';
			
			// save to file, update info and log !!BASE64!!
			o.status[id].driver.photo = photo.type;
			fs.writeFile('assets/data/tracker/'+id+'/driver.'+photo.type, photo.data);
			log.write('Vehicle ['+id+'] driver photo updated');
		}
		return 'ok';
	};



	// renew vehicle lat, lon
	o.updatePos = function(id, pos) {

		// reuse old value if not provided
		var opos = o.status[id].pos;
		if(!pos.lat) pos.lat = opos.lat;
		if(!pos.lon) pos.lon = opos.lon;

		// calculate speed (m/s) & angle (rad)
		if(!pos.speed) pos.speed = o.calcDist(pos, opos) / ((pos.time<opos.time)? pos.time+86400-opos.time : pos.time-opos.time);
		if(!pos.angle) pos.angle = Math.atan2(pos.lat-opos.lat, pos.lon-opos.lon);

		// save pasition info
		_.merge(o.status[id].pos, pos);
		tank.add(o.history[id], pos);
		return 'ok';
	};



	// add new vehicle
	o.add = function(id, status) {

		// check if id is valid
		if(typeof id !== 'string' || id.search(/[^a-z0-9\-]/) >= 0) return 'invalid id';
		if(o.status[id]) return 'already exists';

		// set status (units in SI)
		o.status[id] = {
			'pos': {
				'lat': 0,
				'lon': 0,
				'time':  0,
				'speed': 0,
				'angle': 0,
				'altitude': 0
			},
			'driver': {
				'name':  '?',
				'photo': null,
				'phone': '?'
			},
			'updateTime': 0
		};
		o.history[id] = [];
		_.merge(o.status[id], status);
		o.status[id].updateTime = moment();
		++o.gstatus.count;

		// log and return
		log.write('Vehicle ['+id+'] added');
		return 'ok';
	};



	// update vehicle info
	o.update = function(id, status) {

		// check if id is valid
		if(typeof id !== 'string' || id.search(/[^a-z0-9\-]/) >= 0) return 'invalid id';
		if(!o.status[id]) return 'not exists';

		// update photo if required
		if(status.driver && status.driver.photo) {
			var ret = o.updatePhoto(id, status.driver.photo);
			if(ret !== 'ok') return ret;
		}

		// update position if required
		if(status.pos) {
			var ret = o.updatePos(id, status.pos);
			if(ret !== 'ok') return ret;
		}

		// update all data, log and return
		_.merge(o.status[id], status);
		o.status[id].updateTime = moment();
		if(_.keys(status).length - (_.has(status, 'pos')? 1 : 0) > 0) {
			log.write('Vehicle ['+id+'] updated.');
        }
		return 'ok';
	};



	// add or update vehicle
	o.addOrUpdate = function(id, status) {

		// try adding
		var ret = o.add(id, status);
		if(ret !== 'already exists') return ret;

		// update and return
		return o.update(id, status);
	};



	// remove a vehicle
	o.remove = function(id) {

		// return error if not esists
		if(typeof id !== 'string' || id.search(/[^a-z0-9\-]/) >= 0) return 'invalid id';
		if(!o.status[id]) return 'not exists';

		// delete vehicle info
		delete o.status[id];
		delete o.history[id];
		--o.gstatus.count;

		// log and return true (removed)
		log.write('Vehicle ['+id+'] removed');
		return 'ok';
	};



	// startup
	o.load();
	setInterval(o.save, config.track.saveTime);



	// !!find!!
	// !!observe unreporting vehicles!!



    // return
    return o;
}];
