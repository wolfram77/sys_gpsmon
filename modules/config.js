/* -------------------------
 * Application Configuration
 * -------------------------
 * 
 * Provides most configuration information used by the application to operate.
 * 
 * License:
 * Gps Mon, Copyright (c) 2010-2014, Subhajit Sahu, All Rights Reserved.
 * see: /LICENSE.txt for details.
 */


module.exports = [function() {
	// initialize
	var o = {};


    o.server = {
        // port number the HTTP server runs on
        'port': process.env.PORT || 80,
        // milliseconds of inactivity before a socket is presumed to have timed out
        'timeout': 0
    };

    
    o.log = {
        // max. no. of logs
        'maxSize': 64
    };
    
    

	o.track = {
        // track file saved to mem
        'file': 'config/data/track.json',
        // interval for saving the track file
        'saveTime': 1*60*1000
    };


	o.photo = {
        // no photo file can be larger than this
        'maxSize': 128*1024
    };


	o.req = {
        // no (json) request should be larger than this
        'maxSize': 8*1024
    };
    
    
    o.dir = {
        // accessible directory
        'public': 'assets/'
    };
    
    
    o.map = {
        // background map images
        'image': [
            ['/img/map/0-0.jpeg', '/img/map/0-1.jpeg'],
            ['/img/map/1-0.jpeg', '/img/map/1-1.jpeg']
        ],
        // map ranges
        'lat': [0, 0.00001],
        'lon': [0, 0.00001],
        'altitude': [0, 2000]
    };

    
	// return
	return o;
}];
