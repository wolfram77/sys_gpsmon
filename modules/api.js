/* ---------------
 * Application API
 * ---------------
 * 
 * Provides a API that can be used to access application data.
 * 
 * License:
 * Gps Mon, Copyright (c) 2010-2014, Subhajit Sahu, All Rights Reserved.
 * see: /LICENSE.txt for details.
 */


module.exports = ['_', 'config', 'log', 'code', 'data', function(_, config, log, code, data) {
	// initialize
	var o = {};



	// process api request
	o.process = function(req, res) {

		var ret = [], qry = '';
		var overflow = false;

		// store data only if in limits
		req.on('data', function(chunk) {
			if(qry.length < config.req.maxSize) qry += chunk;
			else overflow = true;
		});

		// parse req array, process, return results
		req.on('end', function() {

			// get json queries [{func, params}, ...]
			try {
				if(overflow) qry = [];
				else qry = JSON.parse(qry);
			}
			catch(e) { qry = []; }

			// process function queries
			for(var i=0; i<qry.length; i++) {
				try {
					if(qry[i].func.search(/[^A-Za-z\.]/) >= 0) ret[i] = 'invalid function';
					else ret[i] = eval('code.'+qry[i].func).apply(this, qry[i].params);
				}
				catch(e) { ret[i] = 'function error'; }
			}

			// return results [ret, ...]
			res.json(ret);
		});
	};



	// fetch from in memory datastore
	o.getData = function(qry) {

		var ret = [];

		// query is an array of string names of variables
		for(var i=0; i<qry.length; i++) {

			// '*' indicates all data in datastore
			if(qry[i] === '*') ret[i] = data;

			// ignore query if contains unwanted chars
			else if(qry[i].search(/[^A-Za-z\.]/) >= 0) ret[i] = {};
			
			// evaluate normal query
			else {
				try { ret[i] = eval('data.'+qry[i]); }
				catch(e) { ret[i] = {}; }
			}
		}
		return ret;
	};



	// return
	return o;
}];
