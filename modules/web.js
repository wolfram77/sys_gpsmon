/* -------------
 * Router (HTTP)
 * -------------
 * 
 * Routes a request path to appropriate handler.
 * 
 * License:
 * Gps Mon, Copyright (c) 2010-2014, Subhajit Sahu, All Rights Reserved.
 * see: /LICENSE.txt for details.
 */


module.exports = ['express', 'log', 'api', 'config', function(express, log, api, config) {
	// initialize
	var web = express();


	// root web page
	web.get('/', function(req, res) {
		res.sendfile('assets/html/index.html');
	});


	// menage web page
	web.get('/manage', function(req, res) {
		res.sendfile('assets/html/manage.html');
	});


	// monitor web page
	web.get('/monitor', function(req, res) {
		res.sendfile('assets/html/monitor.html');
	});


	// handle api request
	web.all('/api', function(req, res) {
		api.process(req, res);
	});


	// credits page
	web.get('/credits', function(req, res) {
		res.sendfile('assets/html/credits.html');
	});


	// disclaimer page
	web.get('/disclaimer', function(req, res) {
		res.sendfile('assets/html/disclaimer.html');
	});


	// static files
	web.use(express.static(config.dir.public));


	// wrong path
	web.use(function(req, res, next) {
		res.status(404).sendfile('assets/html/404.html');
	});


	// return
	return web;
}];
