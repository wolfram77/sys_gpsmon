/* ------
 * Logger
 * ------
 * 
 * Provides a log module that can be used to log information. Multiple
 * types of logs can be used but all logs are output into the console.
 * 
 * License:
 * Gps Mon, Copyright (c) 2010-2014, Subhajit Sahu, All Rights Reserved.
 * see: /LICENSE.txt for details.
 */



module.exports = ['moment', 'config', function(moment, config) {
	// initialize
	var o = {};


    // log strings
	o.data = [];


	// clear logs
	o.clear = function() {
		o.data = [];
	};


	// write a log message
	o.write = function(msg) {

		// if log data is full, remove oldest log
		if(o.data.length >= config.log.maxSize) o.data.shift();

		// format log message, save, display
		var logMsg = '['+moment().format('h:mm:ss a')+'] '+msg+'. ';
		o.data[o.data.length] = logMsg;
		console.log(logMsg);
	}


	// return
	return o;
}];
