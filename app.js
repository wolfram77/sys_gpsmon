/* ----------------
 * Main Application
 * ----------------
 * 
 * This is the main application file.
 * 
 * License:
 * Gps Mon, Copyright (c) 2010-2014, Subhajit Sahu, All Rights Reserved.
 * see: /LICENSE.txt for details.
 */



// initialization
var code = {'api': {}, 'tracker': {}};
var data = {'tracker': {}};

// library loader
var _  = require('./modules/lodash')();
o = {'_': _, 'code': code, 'data': data};

// load libraries
var fs      = _.load('fs', o);
var moment  = _.load('moment', o);
var express = _.load('express', o);
var config  = _.load('./config', o);
var tank    = _.load('./tank', o);
var log     = _.load('./log', o);
var tracker = _.load('./tracker', o);
var api     = _.load('./api', o);
var web     = _.load('./web', o);

// interface
data.log = log.data;
data.config = config;
_.assignOnly(code.api, api, ['getData']);
_.assignOnly(code.tracker, tracker, ['add', 'update', 'addOrUpdate', 'remove']);
_.assignOnly(data.tracker, tracker, ['status', 'history', 'gstatus']);



// create http server
var server = web.listen(config.server.port, function() {

	// log application info on startup
	fs.readFile('config/app.txt', function(err, data) {
		console.log(data.toString());
		log.write('Started on port: '+config.server.port);
	});
});
