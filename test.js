/* ----------------
 * Test Application
 * ----------------
 * 
 * This is the test application file for testing Gps Mon application.
 * 
 * License:
 * Gps Mon, Copyright (c) 2010-2014, Subhajit Sahu, All Rights Reserved.
 * see: /LICENSE.txt for details.
 */



// load libraries
var http = require('http');


// initialize
var o = {};

// map x, y limits
o.map = {};
o.map.x = [0, 1];
o.map.y = [0, 1];

// request options
o.req = {};
o.req.options = {
	'host': '127.0.0.1',
	'method': 'POST',
	'path': '/api',
	'headers': {
		'content-type': 'application/json'
	}
};


// get details from number
o.details = function(num) {
	var str = ''+num;
	for(var i=str.length; i<4; i++)
		str = '0'+str;
	var status = {};
	status.driver = {
		'name': 'name'+num,
		'phone': '999999'+str
	};
	return {
		'id': 'or-00a-'+str,
		'status': status
	};
};


// api request
o.apiReq = function(func, params) {
	var req = http.request(o.req.options);
	req.write(JSON.stringify([{
		'func': func,
		'params': params
	}]));
	req.end();
};


// add vehicles
o.add = function(num) {
	for(var i=0; i<num; i++) {
		var info = o.details(i);
		o.apiReq('tracker.add', [info.id, info.status]);
	}
};


// remove vehicles
o.remove = function(num) {
	for(var i=0; i<num; i++) {
		var info = o.details(i);
		o.apiReq('tracker.remove', [info.id]);
	}
};


// update vehicles
o.update = function(num) {
	for(var i=0; i<num; i++) {
		var info = o.details(i);
		var status = {
			'pos': {
				'lat': 0.00001*Math.random(),
				'lon': 0.00001*Math.random(),
				'time': process.hrtime()[0]
			}
		};
		o.apiReq('tracker.update', [info.id, status]);
	}
};


// run
setInterval(function() {
	console.log('Updating 20 vehicles');
	o.update(20);
}, 5*1000);
