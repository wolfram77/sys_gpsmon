/* --------------------
 * Main Frontend Script
 * --------------------
 * 
 * This is the main frontend javascript file.
 * 
 * License:
 * Gps Mon, Copyright (c) 2010-2014, Subhajit Sahu, All Rights Reserved.
 * see: /LICENSE.txt for details.
 */



// module definition
var web = angular.module('web', []);



// menu controller
web.controller('menuCtrl', ['$scope', function($scope) {

	// initialize
	var o = $scope;
	o.value = 0;

	// set menu value
	o.set = function(val) {
		o.value = val;
	}

	// get menu value
	o.get = function() {
		return o.value;
	}

	// is selected?
	o.is = function(val) {
		return o.value === val;
	}
}]);



// web-footer element directive
web.directive('webFooter', function() {
	return {
		restrict: 'E',
		templateUrl: '/html/web-footer.html'
	};
});



// web-header element directive
web.directive('webHeader', function () {
	return {
		scope: true,
		restrict: 'E',
		templateUrl: '/html/web-header.html',
		controller: 'menuCtrl'
	};
});



// tracker controller
web.controller('trackerCtrl', ['$scope', '$http', function($scope, $http) {

	// initialize
	var o = $scope;
	o.data = {};
	o.info = {};
	o.msg = null;



	// clear temp data
	o.clear = function() {
		o.data = {};
		o.info = {};
	};



	// display message
	o.message = function(msg) {
		o.msg = msg;
		setTimeout(function() {
			o.msg = null;
		}, 5*1000);
	};



	// check vehicle id
	o.checkId = function(_id) {
		var id = _id.toLowerCase().replace(/ /g, '-');
		if(id.length === 11 && id.search(/[^a-z0-9\-]/) < 0 &&
			id.search(/[a-z]/) >= 0 && id.search(/[0-9]/) >= 0) return id;
		return null;
	};



	// request api
	o.apiReq = function(func, params, callback) {

		// post query
		$http.post('/api', [{
			'func': func,
			'params': params

		// check if success and respond
		}]).success(function(res) {
			callback(res[0]);

		// report network failure
		}).error(function() {
			o.message('Network connectivity problem.');
		});
	};



	// refresh status
	o.refresh = function() {

		// request tracker status (local & global)
		o.apiReq('api.getData', [['tracker.status', 'tracker.gstatus']], function(res) {

			// initialize
			o.status = res[0];
			o.gstatus = res[1];

			// conversion factors
			var ms2kmhr = 3.6;
			var rad2deg = 57.2957795;

			// format info
			for(var i in o.status) {
				var e = o.status[i];
				e.pos.lat *= rad2deg;
				e.pos.lon *= rad2deg;
				e.pos.angle *= rad2deg;
				e.pos.speed *= ms2kmhr;
				e.updateTime = moment(e.updateTime).fromNow();
			}
		});
	};



	// add vehicle
	o.add = function() {

		// check id
		var id = o.checkId(o.info.id);
		if(!id) {
			o.message('Invalid vehicle number.');
			return;
		}

		// request api
		o.apiReq('tracker.add', [id, {}], function(res) {
			if(res !== 'ok') o.message('Failed to add vehicle ['+id+'] ('+res+').');
			else {
				o.message('Vehicle ['+id+'] was added successfully.');
				o.clear();
			}
		});
	};



	// update vehicle
	o.update = function() {

		// conversion factors
		var kmhr2ms = 5/18;
		var deg2rad = 0.0174532925;

		// format data
		if(o.data.pos) {
			var e = o.data.pos;
			if(e.lat) e.lat *= deg2rad;
			if(e.lon) e.lon *= deg2rad;
			if(e.angle) e.angle *= deg2rad;
			if(e.speed) e.speed *= kmhr2ms;
		}

		// request api
		var id = o.info.id;
		o.apiReq('tracker.update', [id, o.data], function(res) {
			if(res !== 'ok') o.message('Failed to update vehicle ['+id+'] ('+res+').');
			else o.message('Vehicle ['+id+'] was updated successfully.');
		});
	};



	// remove vehicle
	o.remove = function() {

		// check if confirmed
		o.info.removeActive = true;
		if(o.info.removeConfirm !== 'remove') return;

		// post query
		var id = o.info.id;
		o.apiReq('tracker.remove', [id], function(res) {
			if(res !== 'ok') o.message('Failed to remove vehicle ['+id+'] ('+res+').');
			else o.message('Vehicle ['+id+'] was removed successfully.');
		});
	};



	// initialize data
	o.refresh();

	// update every 5 seconds
	setInterval(o.refresh, 5*1000);
}]);



// monitor controller
web.controller('monitorCtrl', ['$scope', '$http', function($scope, $http) {

	// initialize
	var o = $scope;
	o.ui = {};
	o.ui.ptr = [];
	o.msg = null;



	// load map
	o.loadMap = function() {
		
		// request configuration
		$http.post('/api', [{
			'func': 'api.getData',
			'params': [['config']]
		}]).success(function(res) {
			
			// initialize
			var map = [];
			var s = o.svg;
			o.config = res[0][0];

			// get map's divisions in rows and columns
			var img = o.config.map.image;
			var rows = img.length, cols = img[0].length;
			var xsz = o.size.x/cols, ysz = o.size.y/rows;

			/*
			// add map parts
			for(var r=0; r<rows; r++) {
				for(var c=0; c<cols; c++) {
					map.push(s.image(img[r][c], c*xsz, r*ysz, xsz, ysz));
				}
			}

			// group map part into ui
			o.ui.map = s.group.apply(s, map);
			*/
		});
	};
	

	
	// intialize map
	o.init = function() {

		// get snap svg object
		var el = '#monitorSvg';
		o.svg = Snap(el);

		// get svg element size
		o.size = {
			'x': $(el).width(),
			'y': $(el).height(),
		};
		o.size.ptr = (o.size.x+o.size.y) / 40;
			
		// create pointer generator
		o.ui.ptrgen = o.svg.group(
			o.svg.circle(-1, -1, 1),
			o.svg.line(-1, -1, 0, -1)
		).attr({
			'fill': '#AEA',
			'stroke': '#7A7',
			'strokeWidth': 0.3,
			'opacity': 0.8
		});

		// load map
		o.loadMap();
	};
	


	// update pointers
	o.update = function() {

		// initialize
		var i = 0;
		var u = o.ui, stat = 0;
		var map = o.config.map;

		// conversion factors
		var rad2deg = 57.2957795;

		// remove unused
		for(i in u.ptr) {
			if(o.status[i]) continue;
			u.ptr[i].remove();
			delete u.ptr[i];
		}

		// add and update
		for(i in o.status) {
			
			// initialize
			stat = o.status[i];

			// add new if required
			if(!u.ptr[i]) u.ptr[i] = u.ptrgen.clone();

			// evaluate property values
			var x = ((stat.pos.lon-map.lon[0]) / (map.lon[1]-map.lon[0])) * o.size.x;
			var y = ((stat.pos.lat-map.lat[0]) / (map.lat[1]-map.lat[0])) * o.size.y;
			var r = stat.pos.angle * rad2deg;
			var w = ((stat.pos.altitude-map.altitude[0]) / (map.altitude[1]-map.altitude[0])) * o.size.ptr + 2;


			o.ui.ptr[i].animate({
				'transform': 'R'+r+',-1,-1S10T'+x+','+y
			}, 6000);
		}
	};



	// refresh status
	o.refresh = function() {

		// post query
		$http.post('/api', [{
			'func': 'api.getData',
			'params': [['tracker.status', 'tracker.gstatus']]
		}]).success(function(res) {
			
			// initialize
			o.status = res[0][0];
			o.gstatus = res[0][1];
			
			// format update time
			for(var i in o.status) {
				o.status[i].updateTime = moment(o.status[i].updateTime).fromNow();
			}
			o.update();
		});
	};



	// initialize data
	o.init();

	// update every 5 seconds
	setInterval(function() {
		o.refresh();
	}, 5*1000);
}]);
